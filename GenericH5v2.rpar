#!/usr/bin/perl -W

use strict;
use FileHandle;
use Getopt::Long qw (:config no_ignore_case bundling);

my $ccerad = 450.;
my $ccepath = "/lustre/datura/ianhin/projects/precbbh/PrecBBH000002_CCE/Lev1";
my $ccefile = "CceR0450.h5";

my $mass = 1.0;     # total mass of the system evolved
my $timestep = 0.1875; # specify the timestep for the Cce evolution.  
                       # typical values range from .3 to .1 for low
                       # to high resolution

my $N_radial_pts = 201; # radial resolution.  typical range is from 
                        # 101 to 301 for low to high resolution.

my $N_ang_pts_inside_eq = 81; # angular resolution.  typical range is
                              # from 41 to 81 for low to high resolution.
                              # be aware that too much resolution may
                              # result in a CFL instability for a given
                              # timestep.

sub Help($@) {
  my ($rc, @opts) = @_;
  my $FH = $rc ? *STDERR : *STDOUT;
  print $FH "Known options are:\n";
  while(my ($opt, $val) = splice(@opts, 0, 2)) {
    print $FH (length($opt) == 1 ? "-" : "--")."$opt".
              (defined $$val ? "=$$val " : " ");
  }
  print $FH "\n";
  exit $rc;
}

my $opt_h = undef;
my @opts = ('h' => \$opt_h,
           'help' => \$opt_h,
           'ccerad=i' => \$ccerad,
           'ccepath=s' => \$ccepath,
           'ccefile=s' => \$ccefile,
           'timestep=f' => \$timestep,
           'mass=f' => \$mass,
           'N_radial_pts=i' => \$N_radial_pts,
           'N_ang_pts_inside_eq=i' => \$N_ang_pts_inside_eq
);
GetOptions(@opts) || Help(1, @opts);

Help(0, @opts) if $opt_h;

my $lines = <<EOF;
#------------------------------------------------------------------------------
# A parfile to do CCE with sperical harmonic coefficients of the Cauchy metric
# from a file.
#
#------------------------------------------------------------------------------

ActiveThorns = "SymBase CoordBase CartGrid3D IOASCII IOBasic IOUtil Pugh PughReduce PughSlab PUGHInterp Time"
ActiveThorns = "Fortran MoL SpaceMask LocalReduce"
ActiveThorns = "ADMBase"
ActiveThorns = "Boundary"
ActiveThorns = "AEILocalInterp NullDecomp NullGrid NullInterp NullVars NullEvolve NullNews NullSHRExtract SphericalHarmonicReconGen"
ActiveThorns = "NaNChecker TimerReport"
ActiveThorns = "IOHDF5 IOHDF5Util TerminationTrigger"

#------------------------------------------------------------------------------

driver::global_nx = 22 
driver::global_ny = 22
driver::global_nz = 22 
driver::ghost_size = 3 

grid::xyzmin = -5
grid::xyzmax = +5
grid::type = "byrange"

cactus::terminate         = "never" # never, time
cactus::cctk_initial_time = 0
cactus::cctk_final_time   = 10.0
cactus::cctk_itlast       = 1
time::timestep_method     = given
time::timestep            = $timestep*$mass
cactus::highlight_warning_messages = no

#------------------------------------------------------------------------------
NullEvolve::boundary_data      = "SHRE"
NullEvolve::dissip_J           = 0.0
NullEvolve::first_order_scheme = yes
NullEvolve::initial_J_data     = "vanishing_J_scri"

NullSHRExtract::cr    = $ccerad # the non-compactified worldtube radius
NullSHRExtract::mass  = 1.0 # not used
NullSHRExtract::l_max = 16 # maximum L in Cce boundary data files
                           # hard-coded to 16 in AddWaveExtraction.cpp's 
                           # mSkDataBoxCceOptions variable


#Interpolation
#-----------------------------------------
NullInterp::interpolation_order = 4
NullInterp::stereo_patch_type   = "circle"
NullInterp::deriv_accuracy      = 4

# Null Grid
#-----------------------------------------
NullGrid::null_rwt     = $ccerad
NullGrid::null_xin     = 0.48
NullGrid::N_radial_pts = $N_radial_pts
NullGrid::N_ang_pts_inside_eq = $N_ang_pts_inside_eq  
NullGrid::N_ang_stencil_size  = 4
NullGrid::N_ang_ev_outside_eq = 2
NullGrid::N_ang_ghost_pts     = 4
#------------------------------------------------------------------------------

NullDecomp::l_max     = 8
NullDecomp::use_rsYlm = no
NullNews::compute_lin_strain = yes
NullNews::write_spherical_harmonics = yes
Nullnews::interp_to_constant_uBondi = yes
NullNews::max_timelevels       = 100
NullNews::use_linearized_omega = yes
NullNews::first_order_scheme   = no

IO::out_dir = \$parfile

IOBasic::outInfo_every        = 1
IOBasic::outInfo_reductions   = "norm_inf"
IOBasic::outScalar_every      = 1
IOBasic::outScalar_reductions = "norm2 norm_inf"
IOASCII::out1D_every          = -1
IOASCII::out2D_every          = -1

IOBasic::outInfo_vars    = "NullNews::NewsB"
IOBasic::outScalar_style = "gnuplot"
IOBasic::outScalar_vars  = "NullNews::uBondi NullVars::bcn[0] NullVars::bcn[20]"

#------------------------------------------------------------------------------

ADMBase::initial_shift = "zero"

#------------------------------------------------------------------------------

NaNChecker::action_if_found = "terminate"
NaNChecker::check_every     = 16
NaNChecker::check_vars      = "
  ADMBASE::lapse ADMBASE::shift ADMBASE::metric ADMBASE::curv
  NULLEVOLVE::Jrad NULLEVOLVE::dxJrad
  NULLNEWS::CNewsArrs NULLNEWS::RNewsArrs
  NULLNEWS::JJ_l 
  NULLNEWS::News NULLNEWS::NewsB NULLNEWS::NewsB_uBondi NULLNEWS::News_uBondi
  NULLNEWS::Psi4 NULLNEWS::Psi4_uBondi
  NULLNEWS::uBondi
  NULLVARS::realcharfuncs
  NULLVARS::cmplxcharfuncs_basic NULLVARS::cmplxcharfuncs_aux
"
NaNChecker::report_max      = 1


#-----------------------------------------------------------------------------
# ASCII input


SphericalHarmonicReconGen::sphere_number = 0 # sphere number counting from 0
SphericalHarmonicReconGen::time_derivative_in_file = yes
SphericalHarmonicReconGen::time_fd_order    = 4
SphericalHarmonicReconGen::time_interpolate = yes
SphericalHarmonicReconGen::verbose          = yes
SphericalHarmonicReconGen::cached_timesteps = 20
SphericalHarmonicReconGen::lmaxInFile       = 16 # only required for dat files


SphericalHarmonicReconGen::path   = "$ccepath"
SphericalHarmonicReconGen::format = "SpEC-H5-v2"

SphericalHarmonicReconGen::file_lapse[0]  = "$ccefile"
SphericalHarmonicReconGen::file_shiftx[0] = "$ccefile"
SphericalHarmonicReconGen::file_shifty[0] = "$ccefile"
SphericalHarmonicReconGen::file_shiftz[0] = "$ccefile"
SphericalHarmonicReconGen::file_gxx[0]    = "$ccefile"
SphericalHarmonicReconGen::file_gxy[0]    = "$ccefile"
SphericalHarmonicReconGen::file_gxz[0]    = "$ccefile"
SphericalHarmonicReconGen::file_gyy[0]    = "$ccefile"
SphericalHarmonicReconGen::file_gyz[0]    = "$ccefile"
SphericalHarmonicReconGen::file_gzz[0]    = "$ccefile"

SphericalHarmonicReconGen::file_lapse[1]  = "$ccefile"
SphericalHarmonicReconGen::file_shiftx[1] = "$ccefile"
SphericalHarmonicReconGen::file_shifty[1] = "$ccefile"
SphericalHarmonicReconGen::file_shiftz[1] = "$ccefile"
SphericalHarmonicReconGen::file_gxx[1]    = "$ccefile"
SphericalHarmonicReconGen::file_gxy[1]    = "$ccefile"
SphericalHarmonicReconGen::file_gxz[1]    = "$ccefile"
SphericalHarmonicReconGen::file_gyy[1]    = "$ccefile"
SphericalHarmonicReconGen::file_gyz[1]    = "$ccefile"
SphericalHarmonicReconGen::file_gzz[1]    = "$ccefile"

SphericalHarmonicReconGen::file_lapse[2]  = "$ccefile"
SphericalHarmonicReconGen::file_shiftx[2] = "$ccefile"
SphericalHarmonicReconGen::file_shifty[2] = "$ccefile"
SphericalHarmonicReconGen::file_shiftz[2] = "$ccefile"
SphericalHarmonicReconGen::file_gxx[2]    = "$ccefile"
SphericalHarmonicReconGen::file_gxy[2]    = "$ccefile"
SphericalHarmonicReconGen::file_gxz[2]    = "$ccefile"
SphericalHarmonicReconGen::file_gyy[2]    = "$ccefile"
SphericalHarmonicReconGen::file_gyz[2]    = "$ccefile"
SphericalHarmonicReconGen::file_gzz[2]    = "$ccefile"


SphericalHarmonicReconGen::column_lapse[0]  = 0
SphericalHarmonicReconGen::column_shiftx[0] = 1
SphericalHarmonicReconGen::column_shifty[0] = 2
SphericalHarmonicReconGen::column_shiftz[0] = 3
SphericalHarmonicReconGen::column_gxx[0]    = 4
SphericalHarmonicReconGen::column_gxy[0]    = 5
SphericalHarmonicReconGen::column_gxz[0]    = 6
SphericalHarmonicReconGen::column_gyy[0]    = 7
SphericalHarmonicReconGen::column_gyz[0]    = 8
SphericalHarmonicReconGen::column_gzz[0]    = 9

# radial derivatives
SphericalHarmonicReconGen::column_lapse[1]  = 10
SphericalHarmonicReconGen::column_shiftx[1] = 11
SphericalHarmonicReconGen::column_shifty[1] = 12
SphericalHarmonicReconGen::column_shiftz[1] = 13
SphericalHarmonicReconGen::column_gxx[1]    = 14
SphericalHarmonicReconGen::column_gxy[1]    = 15
SphericalHarmonicReconGen::column_gxz[1]    = 16
SphericalHarmonicReconGen::column_gyy[1]    = 17
SphericalHarmonicReconGen::column_gyz[1]    = 18
SphericalHarmonicReconGen::column_gzz[1]    = 19

# time derivatives
SphericalHarmonicReconGen::column_lapse[2]  = 20
SphericalHarmonicReconGen::column_shiftx[2] = 21
SphericalHarmonicReconGen::column_shifty[2] = 22
SphericalHarmonicReconGen::column_shiftz[2] = 23
SphericalHarmonicReconGen::column_gxx[2]    = 24
SphericalHarmonicReconGen::column_gxy[2]    = 25
SphericalHarmonicReconGen::column_gxz[2]    = 26
SphericalHarmonicReconGen::column_gyy[2]    = 27
SphericalHarmonicReconGen::column_gyz[2]    = 28
SphericalHarmonicReconGen::column_gzz[2]    = 29


### Checkpointing

IOHDF5::checkpoint     = yes
IO::checkpoint_ID      = no
IO::recover            = autoprobe
IO::recover_dir        = \$parfile
IO::checkpoint_on_terminate = yes
IO::checkpoint_keep    = 3
IO::checkpoint_dir     = \$parfile
IO::abort_on_io_errors = yes
TerminationTrigger::on_remaining_walltime = 30.0
TerminationTrigger::max_walltime = \@WALLTIME_HOURS\@
EOF

# my $filename = "$0";
# $filename =~ s/\.rpar/.par/g;
#
# open(OUT,">$filename");
# print OUT "$lines";
# close(OUT);
print "$lines";

