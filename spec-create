#!/bin/bash

# Create a SpEC BBH simulation with the given parameters

# Example:
#
#  ./spec-create --spec-home /home/user/SpEC --directory $SCRATCH/simulations/empe --name empe_015 --ReduceEcc 0 --q 1 --chiA 0,0,0 --chiB 0,0,0 --Omega0 0.022453928379560246 --adot0 -0.0007581018270662672 --D0 12.081385772569398 
#

set -e
set -u

opt_directory="."
opt_submit=1
opt_ReduceEcc=0

while [[ $# -gt 0 ]]; do
    opt="$1"
    if [[ ! "$opt" =~ ^--.* ]]; then
       echo "WARNING: $opt does not start with '--'"
    fi
    shift

    current_arg="$1"
    if [[ "$current_arg" =~ ^--.* ]]; then
        echo "WARNING: $current_arg starts with '--'; is there an option value missing?" 
    fi

    case "$opt" in
        "--spec-home") opt_spec_home="$1"; shift;;
        "--q")         opt_q="$1"; shift;;
        "--NOrbits")   opt_NOrbits="$1"; shift;;
        "--chiA")      opt_chiA="$1"; shift;;
        "--chiB")      opt_chiB="$1"; shift;;
        "--Omega0")    opt_Omega0="$1"; shift;;
        "--adot0")     opt_adot0="$1"; shift;;
        "--D0")        opt_D0="$1"; shift;;
        "--name")      opt_name="$1"; shift;;
        "--ReduceEcc") opt_ReduceEcc="$1"; shift;;
        "--directory") opt_directory="$1"; shift;;
        "--submit")    opt_submit="$1"; shift;;
        "--priority")  opt_priority="$1"; shift;;
        "--FinalTime") opt_final_time="$1"; shift;;
        "--MinLev")    opt_min_lev="$1"; shift;;
        "--MaxLev")    opt_max_lev="$1"; shift;;
        *)             echo "ERROR: Invalid option: \""$opt"\"" >&2
                       exit 1;;
    esac
done

name=$opt_name

if [ $opt_ReduceEcc = 1 ]; then
    eccreduce=--reduce-ecc
else
    eccreduce=--no-reduce-ecc
fi

simdir=$opt_directory/$name
if [ -r $simdir ]; then
    echo "Skipping existing simulation $simdir"
    exit 0
fi

mkdir $simdir

cd $simdir

prepare_id_args=(-t bbh2 $eccreduce)

# This needs a SpEC commit which is not on master yet
if [ ! -z ${opt_priority+x} ]; then
    prepare_id_args+=(-priority $opt_priority)
fi

source $opt_spec_home/MakefileRules/this_machine.env
$opt_spec_home/Support/bin/PrepareID "${prepare_id_args[@]}"

if [ $opt_ReduceEcc = 1 ]; then
    cd Ecc0
fi

if [ ! -z ${opt_NOrbits+x} ]; then
    # This requires a commit to SpEC which is not yet on master
    outfile=ZeroEccParamsFromPN.out
    $opt_spec_home/Support/Python/ZeroEccParamsFromPN.py --WriteParams Params.input --q $opt_q --NOrbits $opt_NOrbits --chiA $opt_chiA --chiB $opt_chiB > $outfile
    # time_to_merger=$(grep timeToMerger $outfile | tail -1 | awk '{print $3}')
    # echo "time_to_merger = $time_to_merger" >&2
    # printf "\n\$EstTimeToMerger = ${time_to_merger};\n" >>Params.input
else
    cat >Params.input <<EOF
# Set the initial data parameters

# Orbital parameters
\$Omega0 = ${opt_Omega0};
\$adot0 = ${opt_adot0};
\$D0 = ${opt_D0};

# Physical parameters
\$MassRatio = ${opt_q};
@SpinA = (${opt_chiA});
@SpinB = (${opt_chiB});

# Evolve after initial data completes?
\$Evolve = 1;
# Use SKS flag? (if false, uses CF)
\$UseSKS = 1;
EOF
fi

DMR=Ev/DoMultipleRuns.input

if [ ! -z ${opt_final_time+x} ]; then
    sed -i 's/^my $FinalTime = .*$/my $FinalTime = '$opt_final_time';/g' ${DMR}
fi

if [ ! -z ${opt_min_lev+x} ]; then
    sed -i 's/^my $MinLev = .*$/my $MinLev = '$opt_min_lev';/g' ${DMR}
fi

if [ ! -z ${opt_max_lev+x} ]; then
    sed -i 's/^my $MaxLev = .*$/my $MaxLev = '$opt_max_lev';/g' ${DMR}
fi    

if [ $opt_submit = 1 ]; then
    ./StartJob.sh
fi
