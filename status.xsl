<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:variable name='projectname' select="document('config.xml')/simulationsconfig/projectname"/> 
  <xsl:template match="div">
    <html>
      <head>
        <link rel="StyleSheet" href="status.css" type="text/css"/>
        <title><xsl:value-of select="$projectname"/></title>

 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 <script>
    $(document).ready(function () {
        $("#searchInput").keyup(function () {
            var val = $(this).val();

            $("#searchTable tr").each(function (i) {
                $(this).hide();

                $("td").each(function (j) {

                var content = $(this).text();
                if (content.toLowerCase().indexOf(val.toLowerCase()) != -1) {
                    $(this).parent().show();
                }
              })
            });
        });
    });
</script>

      </head>
      <body>
        <h1><xsl:value-of select="$projectname"/></h1>
        <h2>Status</h2>
        <xsl:copy>
          <xsl:attribute name="id">searchTable</xsl:attribute>
          <xsl:apply-templates/>
        </xsl:copy>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="td[text()='RUNNING']">
    <xsl:copy>
      <xsl:attribute name="class">running</xsl:attribute>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="td[text()='Failed']">
    <xsl:copy>
      <xsl:attribute name="class">failed</xsl:attribute>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
