# SpEC Scripts 2

A set of scripts for managing SpEC simulations.  To install, simply make sure that the scripts can be
found on your PATH.

Documentation can be found at https://bitbucket.org/ianhinder/specscripts2/wiki/.